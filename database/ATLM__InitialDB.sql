IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [EmailLogs] (
    [Id] int NOT NULL IDENTITY,
    [DateCreated] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsActive] bit NOT NULL,
    [Sender] nvarchar(1000) NOT NULL,
    [Receiver] nvarchar(1000) NOT NULL,
    [CC] nvarchar(1000) NULL,
    [BCC] nvarchar(max) NULL,
    [Subject] nvarchar(max) NOT NULL,
    [MessageBody] nvarchar(max) NOT NULL,
    [Retires] int NOT NULL,
    [IsSent] bit NOT NULL,
    [DateSent] datetime2 NULL,
    [DateToSend] datetime2 NOT NULL,
    CONSTRAINT [PK_EmailLogs] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [ResponseLogs] (
    [Id] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsActive] bit NOT NULL,
    [VigiPayRef] nvarchar(max) NULL,
    [Response] nvarchar(max) NULL,
    [RequestType] int NOT NULL,
    CONSTRAINT [PK_ResponseLogs] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [TransactionLogs] (
    [Id] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NOT NULL,
    [IsDeleted] bit NOT NULL,
    [IsActive] bit NOT NULL,
    [VigipayReference] nvarchar(max) NOT NULL,
    [ProviderReferenceNumber] nvarchar(max) NULL,
    [ProvidereReferenceNumber] nvarchar(max) NULL,
    [ProviderFXRate] nvarchar(max) NULL,
    [SenderFirstName] nvarchar(max) NOT NULL,
    [SenderLastName] nvarchar(max) NOT NULL,
    [SenderCountry] nvarchar(max) NULL,
    [SenderEmail] nvarchar(max) NULL,
    [SenderPhoneNumber] nvarchar(max) NULL,
    [SenderAddress] nvarchar(max) NULL,
    [SenderCity] nvarchar(max) NULL,
    [SendAmount] decimal(18,2) NOT NULL,
    [SendCurrency] nvarchar(max) NOT NULL,
    [SenderDocument] nvarchar(max) NOT NULL,
    [SenderDocumentIDNumber] nvarchar(max) NOT NULL,
    [SenderDocumentIssuedDate] nvarchar(max) NOT NULL,
    [SenderDocumentExpiryDate] nvarchar(max) NOT NULL,
    [BeneficiaryFirstName] nvarchar(max) NOT NULL,
    [BeneficiaryLastName] nvarchar(max) NOT NULL,
    [BeneficiaryPhoneNumber] nvarchar(max) NULL,
    [BeneficiaryCountry] nvarchar(max) NULL,
    [BeneficiaryAddress] nvarchar(max) NULL,
    [BeneficiaryEmail] nvarchar(max) NULL,
    [BeneficiaryMobileOperator] nvarchar(max) NULL,
    [BeneficiaryBankRoutingNumber] nvarchar(max) NULL,
    [BeneficiaryBankAccountNumber] nvarchar(max) NULL,
    [BeneficiaryCurrency] nvarchar(max) NOT NULL,
    [ServiceType] nvarchar(max) NOT NULL,
    [TransactionReference] nvarchar(max) NOT NULL,
    [TransactionStatus] int NOT NULL,
    [StatusDescription] nvarchar(max) NULL,
    [GatewayResponseCode] nvarchar(max) NULL,
    [GatewayResponseMessage] nvarchar(max) NULL,
    [ResponseCode] nvarchar(max) NULL,
    [ResponseMessage] nvarchar(max) NULL,
    [RequestString] nvarchar(max) NULL,
    [ResponseString] nvarchar(max) NULL,
    [RequeryCount] int NOT NULL,
    [RequeryResponseString] nvarchar(max) NULL,
    CONSTRAINT [PK_TransactionLogs] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190627120830_initial_migration', N'2.2.4-servicing-10062');

GO

