﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VigiPay.Orbit.Data.EntityBase;

namespace VigiPay.Orbit.Data.AppEntity
{
    public class EmailLog : Entity<int>
    {
        public EmailLog()
        {
            Retires = 0;
            IsSent = false;
        }

        [Required]
        [StringLength(1000)]
        public string Sender { get; set; }
        [Required]
        [StringLength(1000)]
        public string Receiver { get; set; }

        [StringLength(1000)]
        public string CC { get; set; }

        public string BCC { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string MessageBody { get; set; }

        public int Retires { get; set; }
        public bool IsSent { get; set; }

        public DateTime? DateSent { get; set; }

        public DateTime DateToSend { get; set; }
    }
}
