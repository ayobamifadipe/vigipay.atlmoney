﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VigiPay.Orbit.Data.EntityBase;

namespace VigiPay.ATLMoney.Data.AppEntity
{
    public enum TransactionStatus
    {
        [Description("Accepted")]
        Accepted = 0,
        [Description("Processing")]
        Processing = 1,
        [Description("Success")]
        Success = 2,
        [Description("Failed")]
        Failed = 3
    }

    public class TransactionLog : Entity<long>
    {

        [Required]
        public string VigipayReference { get; set; } //vigipay's unique ref

        public string ProviderReferenceNumber { get; set; }
        public string ProvidereReferenceNumber { get; set; }
        public string ProviderFXRate { get; set; }

        [Required]
        public string SenderFirstName { get; set; }
        [Required]
        public string SenderLastName { get; set; }
        public string SenderCountry { get; set; }
        public string SenderEmail { get; set; }

        public string SenderPhoneNumber { get; set; }
        public string SenderAddress { get; set; }
        public string SenderCity { get; set; }
        [Required]
        public decimal SendAmount { get; set; }
        [Required]
        public string SendCurrency { get; set; }
        [Required]
        public string SenderDocument { get; set; }
        [Required]
        public string SenderDocumentIDNumber { get; set; }
        [Required]
        public string SenderDocumentIssuedDate { get; set; }
        [Required]
        public string SenderDocumentExpiryDate { get; set; }

        //Receiver
        [Required]
        public string BeneficiaryFirstName { get; set; }
        [Required]
        public string BeneficiaryLastName { get; set; }
        public string BeneficiaryPhoneNumber { get; set; }
        public string BeneficiaryCountry { get; set; }
        public string BeneficiaryAddress { get; set; }
        public string BeneficiaryEmail { get; set; }

        public string BeneficiaryMobileOperator { get; set; }
        public string BeneficiaryBankRoutingNumber { get; set; }
        public string BeneficiaryBankAccountNumber { get; set; }

        [Required]
        public string BeneficiaryCurrency { get; set; }
        [Required]
        public string ServiceType { get; set; }
        [Required]
        public string TransactionReference { get; set; }
        public TransactionStatus TransactionStatus { get; set; }

        public string StatusDescription { get; set; }
        public string GatewayResponseCode { get; set; }
        public string GatewayResponseMessage { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string RequestString { get; set; }
        public string ResponseString { get; set; }
        public int RequeryCount { get; set; } //count number of requeries
        public string RequeryResponseString { get; set; }
    }
}
