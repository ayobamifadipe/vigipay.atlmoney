﻿using System;
using System.Collections.Generic;
using System.Text;
using VigiPay.Orbit.Data.EntityBase;

namespace VigiPay.ATLMoney.Data.AppEntity
{
    public enum EnumReQuestType
    {
        PayOut = 1,
        ReQuery
    }
    public class ResponseLog : Entity<long>
    {
        public ResponseLog()
        {
            DateCreated = DateTime.Now;
        }
        public string VigiPayRef { get; set; }
        public string Response { get; set; }
        public EnumReQuestType RequestType { get; set; }
    }
}
