﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Data.AppViewModel
{
    public class ATLMoneySetting
    {
        public string BaseURL { get; set; }
        public string Token { get; set; }
    }
}
