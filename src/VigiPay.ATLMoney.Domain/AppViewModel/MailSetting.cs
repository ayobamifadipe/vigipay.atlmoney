﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Data.AppViewModel
{
    public class MailSetting
    {
        public string MailFrom { get; set; }
        public string SMTPServer { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPPORT { get; set; }
    }
}
