﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Data.AppViewModel
{
    public class AmazaonS3APPSetting
    {
        public string S3ApplicationFolder { get; set; }
        public string S3BucketName { get; set; }
        public string S3BucketServiceURL { get; set; }
    }
}
