﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VigiPay.ATLMoney.Data.Migrations
{
    public partial class initial_migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmailLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Sender = table.Column<string>(maxLength: 1000, nullable: false),
                    Receiver = table.Column<string>(maxLength: 1000, nullable: false),
                    CC = table.Column<string>(maxLength: 1000, nullable: true),
                    BCC = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: false),
                    MessageBody = table.Column<string>(nullable: false),
                    Retires = table.Column<int>(nullable: false),
                    IsSent = table.Column<bool>(nullable: false),
                    DateSent = table.Column<DateTime>(nullable: true),
                    DateToSend = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResponseLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VigiPayRef = table.Column<string>(nullable: true),
                    Response = table.Column<string>(nullable: true),
                    RequestType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResponseLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    VigipayReference = table.Column<string>(nullable: false),
                    ProviderReferenceNumber = table.Column<string>(nullable: true),
                    ProvidereReferenceNumber = table.Column<string>(nullable: true),
                    ProviderFXRate = table.Column<string>(nullable: true),
                    SenderFirstName = table.Column<string>(nullable: false),
                    SenderLastName = table.Column<string>(nullable: false),
                    SenderCountry = table.Column<string>(nullable: true),
                    SenderEmail = table.Column<string>(nullable: true),
                    SenderPhoneNumber = table.Column<string>(nullable: true),
                    SenderAddress = table.Column<string>(nullable: true),
                    SenderCity = table.Column<string>(nullable: true),
                    SendAmount = table.Column<decimal>(nullable: false),
                    SendCurrency = table.Column<string>(nullable: false),
                    SenderDocument = table.Column<string>(nullable: false),
                    SenderDocumentIDNumber = table.Column<string>(nullable: false),
                    SenderDocumentIssuedDate = table.Column<string>(nullable: false),
                    SenderDocumentExpiryDate = table.Column<string>(nullable: false),
                    BeneficiaryFirstName = table.Column<string>(nullable: false),
                    BeneficiaryLastName = table.Column<string>(nullable: false),
                    BeneficiaryPhoneNumber = table.Column<string>(nullable: true),
                    BeneficiaryCountry = table.Column<string>(nullable: true),
                    BeneficiaryAddress = table.Column<string>(nullable: true),
                    BeneficiaryEmail = table.Column<string>(nullable: true),
                    BeneficiaryMobileOperator = table.Column<string>(nullable: true),
                    BeneficiaryBankRoutingNumber = table.Column<string>(nullable: true),
                    BeneficiaryBankAccountNumber = table.Column<string>(nullable: true),
                    BeneficiaryCurrency = table.Column<string>(nullable: false),
                    ServiceType = table.Column<string>(nullable: false),
                    TransactionReference = table.Column<string>(nullable: false),
                    TransactionStatus = table.Column<int>(nullable: false),
                    StatusDescription = table.Column<string>(nullable: true),
                    GatewayResponseCode = table.Column<string>(nullable: true),
                    GatewayResponseMessage = table.Column<string>(nullable: true),
                    ResponseCode = table.Column<string>(nullable: true),
                    ResponseMessage = table.Column<string>(nullable: true),
                    RequestString = table.Column<string>(nullable: true),
                    ResponseString = table.Column<string>(nullable: true),
                    RequeryCount = table.Column<int>(nullable: false),
                    RequeryResponseString = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionLogs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailLogs");

            migrationBuilder.DropTable(
                name: "ResponseLogs");

            migrationBuilder.DropTable(
                name: "TransactionLogs");
        }
    }
}
