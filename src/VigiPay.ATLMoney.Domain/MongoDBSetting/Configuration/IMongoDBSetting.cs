﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Data
{
    interface IMongoDBSetting
    {
        string ConnectionString { get; set; }

        string DatabaseName { get; set; }
    }
}
