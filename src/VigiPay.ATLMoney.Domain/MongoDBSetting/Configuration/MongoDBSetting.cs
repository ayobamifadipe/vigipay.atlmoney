﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Data
{
    public class MongoDBSetting : IMongoDBSetting
    {
        public string ConnectionString { get; set; }

        public string DatabaseName { get; set; }
    }
}
