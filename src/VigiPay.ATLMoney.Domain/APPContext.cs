﻿
using System;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using VigiPay.Orbit.Data.EntityContract;
using VigiPay.Orbit.Data.EntityBase;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Collections.Generic;
using VigiPay.Orbit.Data.AppEntity;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Net;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using VigiPay.ATLMoney.Data.AppEntity;

namespace VigiPay.Orbit.Data
{
    public class APPContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMongoDatabase _database = null;

        #region MyDBSetRegion
        public DbSet<EmailLog> EmailLogs { get; set; }
        public DbSet<ResponseLog> ResponseLogs { get; set; }
        public DbSet<TransactionLog> TransactionLogs { get; set; }
        #endregion

        //public APPContext(DbContextOptions<APPContext> options, IHttpContextAccessor httpContextAccessor,IOptions<MongoDBSetting> settings)
        //    : base(options)
        //{
        //    var client = new MongoClient(settings.Value.ConnectionString);
        //    if (client != null)
        //        _database = client.GetDatabase(settings.Value.DatabaseName);
        //    _httpContextAccessor = httpContextAccessor;
        //}


        public APPContext(DbContextOptions<APPContext> options,IHttpContextAccessor httpContextAccessor)
          : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private IMongoCollection<AuditLog> AuditLog
        {
            get
            {
                return _database.GetCollection<AuditLog>("AuditLog");
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        #region MyOverRideSaveChangeRegion
        public override int SaveChanges()
        {

            try
            {
               // Audits();
                return base.SaveChanges();
            }
            catch  { return 0; }

        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
               // Audits();
                return await base.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException filterContext)
            {
                Debug.WriteLine("Concurrency Error: {0}", filterContext.Message);
                return await Task.FromResult(0);

            }

        }
        #endregion

        #region MyDateCreated&DateUpdateRegion
        private void Audits()
        {
            var entities = ChangeTracker.Entries().Where(x =>(x.State == EntityState.Added || x.State == EntityState.Modified));
            long userId = 0;
            try
            {
                userId =long.Parse(_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                foreach (var entity in entities)
                {
                    foreach (var log in GetAuditRecordsForChange(entity, userId))
                    {
                        if(log != null)
                        {
                            //AuditLogs.Add(log);
                            AuditLog.InsertOne(log);
                        }
                    }
                }
            }
            catch { }
        }
        #endregion

        #region MyAuditTrailHelperRegion
        private void SetModifiedProperties(EntityEntry entry, out string oldData, out string newData)
        {
            string json = string.Empty;
            List<AuditEntityProperties> auditEntitiesmodel = new List<AuditEntityProperties>();
            try
            {
                PropertyValues dbValues = entry.GetDatabaseValues();
                foreach (var propertyName in entry.OriginalValues.Properties)
                {
                    var oldVal = dbValues[propertyName.Name];
                    //to get dbValue
                    if (oldVal != null)
                    {
                        if (json.Length > 0)
                        {
                            json += ", ";
                        }
                        json += $@"""{propertyName}"":{(oldVal == null ? "null" : (IsNumber(oldVal) ? oldVal.ToString() : $@"""{oldVal.ToString().Replace("\"", "'")}"""))}";
                    }
                    //to get modified values
                    if (propertyName.Name != "RowVersion")
                    {
                        var newVal = entry.CurrentValues[propertyName];
                        if (oldVal != null && newVal != null && !Equals(oldVal, newVal))
                        {
                            AuditEntityProperties singleauditEntitymodel = new AuditEntityProperties();
                            singleauditEntitymodel.PropertyName = propertyName.Name;
                            singleauditEntitymodel.OldValue = oldVal.ToString();
                            singleauditEntitymodel.NewValue = newVal.ToString();
                            auditEntitiesmodel.Add(singleauditEntitymodel);
                        }
                    }

                }
                oldData = $"{{ {json} }}";
                newData = JsonConvert.SerializeObject(auditEntitiesmodel, Formatting.Indented,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
            catch
            {
                oldData = $"{{ {json} }}";
                newData = string.Empty;
            }
        }
        private static string GetAsJson(PropertyValues values)
        {
            string json = string.Empty;
            try
            {
                if (values != null)
                {
                    foreach (var propertyName in values.Properties)
                    {

                        var val = values[propertyName.Name];
                        if (val != null)
                        {
                            if (json.Length > 0)
                            {
                                json += ", ";
                            }
                            json += $@"""{propertyName}"":{(val == null ? "null" : (IsNumber(val) ? val.ToString() : $@"""{val.ToString().Replace("\"", "'")}"""))}";
                        }

                    }
                }
            }
            catch { }
            return $"{{ {json} }}";
        }
        public static bool IsNumber(object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }
        #endregion
        
        #region MyAuditHelperRegion
        private List<AuditLog> GetAuditRecordsForChange(EntityEntry dbEntry,long userId)
        {
            List<AuditLog> result = new List<AuditLog>();
            string IPAddress = string.Empty;
            try
            {
                try
                {
                    IPHostEntry heserver = Dns.GetHostEntry(Dns.GetHostName());
                    IPAddress = heserver.AddressList.ToList().Where(p => p.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).FirstOrDefault().ToString();
                }
                catch
                {
                    IPAddress = "1::";
                }
                TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), false).SingleOrDefault() as TableAttribute;
                string tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;

                if (tableName.ToUpper().Trim() != "ActivityLog".ToUpper().Trim())
                {
                    string jsonstring = string.Empty;
                    try
                    {
                        jsonstring = JsonConvert.SerializeObject(dbEntry.Entity, Formatting.Indented,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                    }
                    catch { }
                    AuditLog auditlogmodel = new AuditLog();
                    auditlogmodel.UserId = userId;
                    auditlogmodel.EventDate = DateTime.Now;
                    auditlogmodel.TableName = tableName;
                    auditlogmodel.ColumnName = string.Empty;
                    auditlogmodel.IPAddress = IPAddress;
                    if (dbEntry.State == EntityState.Added)
                    {
                        auditlogmodel.EventType = Convert.ToInt32(AuditActionType.Create);
                        auditlogmodel.OldValue = "{  }";
                        auditlogmodel.NewValue = jsonstring;
                    }
                    else if (dbEntry.State == EntityState.Deleted)
                    {
                        auditlogmodel.EventType = Convert.ToInt32(AuditActionType.Delete);
                        auditlogmodel.OldValue = "{  }";
                        auditlogmodel.NewValue = jsonstring;
                    }
                    else if (dbEntry.State == EntityState.Modified)
                    {
                        var oldValues = string.Empty;
                        var modifiedValues = string.Empty;
                        SetModifiedProperties(dbEntry, out oldValues, out modifiedValues);
                        auditlogmodel.EventType = Convert.ToInt32(AuditActionType.Edit);
                        auditlogmodel.OldValue = oldValues;
                        auditlogmodel.NewValue = jsonstring;
                        auditlogmodel.ModifiedValue = modifiedValues;
                    }
                    result.Add(auditlogmodel);
                }
                return result;

            }
            catch
            {
                return result;
            }
        }
        #endregion

    }
}
