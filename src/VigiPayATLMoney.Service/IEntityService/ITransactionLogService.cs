﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VigiPay.ATLMoney.Data.AppEntity;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.Orbit.Service;

namespace VigiPay.ATLMoney.Service.IEntityService
{
    public interface ITransactionLogService : IAutoDependencyService
    {
        bool ClientTransactionRefExists(string clientTransactionRef);
        TransactionLog GetSingle(Expression<Func<TransactionLog, bool>> predicate);
        List<TransactionLog> GetAll(Expression<Func<TransactionLog, bool>> predicate);
        IQueryable<TransactionLog> GetAll();
        TransactionLog Create(PayoutVm model);
        void Update(TransactionLog model);
        void SaveLog(ResponseLog model);
    }
}
