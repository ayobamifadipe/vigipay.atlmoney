﻿using System;
using System.Collections.Generic;
using System.Text;
using VigiPay.Orbit.Data.AppEntity;

namespace VigiPay.Orbit.Service.IEntityService
{
    public interface IAuditLogService : IAutoDependencyService
    {
        void SavLog(AuditLog model);
    }
}
