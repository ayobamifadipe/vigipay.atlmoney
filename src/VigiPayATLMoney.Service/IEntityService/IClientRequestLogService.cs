﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VigiPay.Orbit.Data.AppEntity;

namespace VigiPay.Orbit.Service.IEntityService
{
    public interface IClientRequestLogService : IAutoDependencyService
    {
        void  SaveClientReQuest(ClientRequestLog model);
    }
}
