﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.ATLMoney.Service.ViewModel.ATLMoney;
using VigiPay.Orbit.Service;
using VigiPay.Orbit.Service.ViewModel;

namespace VigiPay.ATLMoney.Service.IEntityService
{
    public interface IATLMoneyService : IAutoDependencyService
    {
        Task<APIResponseModel<SettlementRateResponseVm>> SettlementRate();
        Task<APIResponseModel<AccountBalancesResponseVm>> AccountBalances();
        Task<APIResponseModel<AccountBalanceByCurrencyVm>> AccountBalanceByCurrency(string currencyCode);
        Task<APIResponseModel<AccountStatementResponseVm>> AccountStatementByCurrency(string currencyCode);

        Task<APIResponseModel<BankResponseVm>> BankByCountryCode(string countryCode);
        Task<APIResponseModel<CollectionPointByCountryCodeResponseVm>> GetCashCollectionPointByCountryCode(string countryCode);

        Task<APIResponseModel<PayoutResponseVm>> PostTransaction(PayoutVm model);


        Task<APIResponseModel<GetTransactionResponseVm>> GetAllTransaction();

        Task<APIResponseModel<TransactionResponseVm>> GetTransaction(string TransactionId);
    }
}
