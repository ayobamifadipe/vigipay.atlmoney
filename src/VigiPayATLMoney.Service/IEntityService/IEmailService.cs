﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Service.IEntityService
{
    public interface IEmailService : IAutoDependencyService
    {
        void EmailConfirmation(string token, string userId, string email, string fullName);
        void ForgetPasswordEmail(string token, string email, string fullName);
    }
}
