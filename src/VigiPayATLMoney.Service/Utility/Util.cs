﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using VigiPay.Orbit.Data;

namespace VigiPay.ATLMoney.Service.Utility
{
    public static class Util
    {

        public static bool InRange(this DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck < endDate;
        }


        public static string DecimalToArbitrarySystem(long decimalNumber, int radix)
        {
            const int BitsInLong = 64;
            const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (radix < 2 || radix > Digits.Length)
                throw new ArgumentException("The radix must be >= 2 and <= " + Digits.Length.ToString());
            if (decimalNumber == 0)
                return "0";
            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];
            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % radix);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / radix;
            }
            string result = new string(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0)
            {
                result = "-" + result;
            }
            return result;
        }

        public static string TimeStampCode(string prefix = "")
        {
            Thread.Sleep(1);
            string stamp = DateTime.Now.ToString("yyMMddHHmmssffffff");
            long num = long.Parse(stamp);
            return prefix + DecimalToArbitrarySystem(num, 36);
        }

        public static string TimeStampGuidCode(string prefix = "VGP")
        {
            string base64Guid = ToShortString(Guid.NewGuid());
            var _result = prefix + base64Guid.Replace("==", "");
            return _result;
        }

        public static string ToShortString(Guid guid)
        {
            var base64Guid = Convert.ToBase64String(guid.ToByteArray());

            // Replace URL unfriendly characters with better ones
            base64Guid = base64Guid.Replace("+", "").Replace("/", "");

            // Remove the trailing ==
            return base64Guid.Substring(0, base64Guid.Length - 2).ToUpper();
        }
        public static string GetDescription(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }


        public static string NIPBankCodeResolve(string regularBankCode)
        {
            switch (regularBankCode)
            {
                case "232": return "000001";
                case "082": return "000002";
                case "214": return "000003";
                case "033": return "000004";
                case "063": return "000005";
                case "301": return "000006";
                case "070": return "000007";
                case "076": return "000008";
                case "023": return "000009";
                case "050": return "000010";
                case "215": return "000011";
                case "221": return "000012";
                case "058": return "000013";
                case "044": return "000014";
                case "057": return "000015";
                case "011": return "000016";
                case "035": return "000017";
                case "032": return "000018";
                case "084": return "000019";
                case "030": return "000020";
                case "068": return "000021";
                case "100": return "000022";
                case "101": return "000023";
                //MFB and Others
                case "559": return "060001";
                case "552": return "070001";
                case "501": return "070002";
                case "551": return "070006";
                case "606": return "070007";
                case "560": return "070008";
                case "401": return "090001";
                case "402": return "090003";
                case "502": return "090004";
                case "523": return "090005";
                case "403": return "090006";
                case "413": return "090107";
                case "314": return "100001";
                case "327": return "100002";
                case "311": return "100003";
                ////case "304": return "100004";
                case "317": return "100005";
                case "306": return "100006";
                case "307": return "100008";
                case "319": return "100010";
                case "320": return "100012";
                case "323": return "100013";
                case "309": return "100014";


                ////case "304": return "100007";



                case "308": return "100016";
                case "322": return "100018";
                case "318": return "100019";
                case "302": return "100021";

                default: return "000";
            }
        }


        public static string NIPBankCodeResolveTest(string regularBankCode)
        {
            switch (regularBankCode)
            {
                case "044": return "999044";
                case "063": return "999063";
                case "050": return "999050";
                case "214": return "999214";
                case "011": return "999011";
                case "058": return "999058";
                case "037": return "999037";
                case "221": return "999221";
                case "068": return "999068";
                case "033": return "999033";
                case "215": return "999215";
                case "035": return "999035";
                case "057": return "999057";
                default: return "000";
            }
        }
        public static string SerializeJson(object txt)
        {
            return JsonConvert.SerializeObject(txt);
        }

        public static T DeserializeJson<T>(string txt)
        {
            return JsonConvert.DeserializeObject<T>(txt);
        }

        public static string SerializeDataXml(object data)
        {
            try
            {
                StringBuilder sbData = new StringBuilder();
                StringWriter swWriter = new StringWriter(sbData);

                var _type = data.GetType();
                XmlSerializer serializer = new XmlSerializer(_type);

                serializer.Serialize(swWriter, data);

                var ms = new MemoryStream();

                var xw = XmlWriter.Create(ms);
                serializer.Serialize(xw, data);

                ms.Seek(0, SeekOrigin.Begin);
                var sr = new StreamReader(ms, Encoding.UTF8);
                string xmlString = sr.ReadToEnd();
                swWriter.Dispose();
                xw.Dispose();

                return xmlString.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", string.Empty);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Deserialize string into object
        /// </summary>
        /// <param name="dataXML">xml string to deserialize</param>
        /// <param name="data">an object of the output type</param>
        /// <returns></returns>
        public static T DeserializeDataXml<T>(string dataXML)
        {
            try
            {
                var _type = typeof(T);
                XmlDocument xDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(_type);
                xDoc.LoadXml(dataXML);
                XmlNodeReader xNodeReader = new XmlNodeReader(xDoc.DocumentElement);
                var data = (T)xmlSerializer.Deserialize(xNodeReader);
                return data;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return default(T);
            }
        }

        public static string Hash512(string toHash)
        {
            SHA512 sha512 = new SHA512CryptoServiceProvider();
            sha512.Initialize();
            Byte[] originalByte = UTF8Encoding.Default.GetBytes(toHash);
            Byte[] encodedByte = sha512.ComputeHash(originalByte);
            sha512.Clear();

            string hash = BitConverter.ToString(encodedByte).Replace("-", "");
            return hash;
        }

        public static object DeserializeDataXml<T>(object decodedMessage)
        {
            throw new NotImplementedException();
        }

        public static string RandonString(int stringlenght, bool mixSpecialCharacters)
        {
            var random = new Random();
            int seed = random.Next(1, int.MaxValue);
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ123456789";
            const string specialCharacters = @"!#$%&*<=>?@[\]_";

            var chars = new char[stringlenght];
            var rd = new Random(seed);

            for (var i = 0; i < stringlenght; i++)
            {
                // If we are to use special characters
                if (mixSpecialCharacters && i % random.Next(3, stringlenght) == 0)
                {
                    chars[i] = specialCharacters[rd.Next(0, specialCharacters.Length)];
                }
                else
                {
                    chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                }
            }

            return new string(chars);
        }
    }
}
