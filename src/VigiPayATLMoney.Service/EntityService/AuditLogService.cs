﻿using System;
using System.Collections.Generic;
using System.Text;
using VigiPay.Orbit.Data.AppEntity;
using VigiPay.Orbit.Repository.MongoDBRepo;
using VigiPay.Orbit.Service.IEntityService;

namespace VigiPay.Orbit.Service.EntityService
{
    public class AuditLogService : IAuditLogService
    {

        private readonly IMongoDBRepository<AuditLog, string> _RequestlogCommand;
        public AuditLogService(IMongoDBRepository<AuditLog, string> RequestlogCommand)
        {
            _RequestlogCommand = RequestlogCommand;
        }

        public void SavLog(AuditLog model)
        {
            _RequestlogCommand.Insert(model);
        }
    }
}
