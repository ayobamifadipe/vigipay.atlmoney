﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Data.AppEntity;
using VigiPay.Orbit.Data.AppViewModel;
using VigiPay.Orbit.Repository.CoreRepositories;
using VigiPay.Orbit.Service.IEntityService;

namespace VigiPay.Orbit.Service.EntityService
{
    public class EmailService : IEmailService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepositoryCommand<EmailLog, int> _emailogRepoQuery;
        private APPURL _settings;
        private MailSetting _mailsettings;
        public EmailService(IHttpContextAccessor httpContextAccessor,
             IHostingEnvironment hostingEnvironment,
             IRepositoryCommand<EmailLog, int> emailogRepoQuery
            , IOptions<APPURL> settings
            , IOptions<MailSetting> mailsettings)
        {
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = hostingEnvironment;
            _emailogRepoQuery = emailogRepoQuery;
            _settings = settings.Value;
            _mailsettings = mailsettings.Value;

        }

        public void EmailConfirmation(string token, string userId, string email, string fullName)
        {
            try
            {
                string projectRootPath = _hostingEnvironment.ContentRootPath;
                string folderPath = Path.Combine(projectRootPath, "~/EmailTemplate/ConfirmationEmail.html");

                var callbackUrl = "";
                callbackUrl = $"{_settings.EmailURL}/#/Confirmation?token={token}&userId={userId}";
                string confirmationEmailPath = Path.Combine(projectRootPath, "EmailTemplate");

                string fileContents = File.ReadAllText(confirmationEmailPath);
                fileContents = fileContents.Replace("##NAME##", $"{fullName}");
                fileContents = fileContents.Replace("##ACTIVATIONLINK##", callbackUrl);
                EmailLog logmodel = new EmailLog();
                logmodel.Receiver = "ayfadipe@gmail.com";
                logmodel.Sender = _mailsettings.MailFrom;
                logmodel.Subject = "Email Verification Notification";
                logmodel.MessageBody = fileContents;
                logmodel.DateCreated = logmodel.DateToSend = DateTime.Now;
                logmodel.IsSent = false;
                bool result = SendMail(logmodel.Subject, logmodel.Receiver, logmodel.MessageBody);
                if (result)
                {
                    logmodel.DateSent = DateTime.Now;
                    logmodel.IsSent = true;
                    logmodel.Retires++;
                }
                else
                {
                    logmodel.IsSent = false;
                    logmodel.Retires++;
                }
                _emailogRepoQuery.Insert(logmodel);
                _emailogRepoQuery.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }


        public void ForgetPasswordEmail(string token, string email, string fullName)
        {
            try
            {
                string projectRootPath = _hostingEnvironment.ContentRootPath;
                string folderPath = Path.Combine(projectRootPath, "~/EmailTemplate/PasswordResetNotification.html");

                var callbackUrl = "";
                callbackUrl = $"{_settings.EmailURL}/#/resetPassword?token={token}";
                string confirmationEmailPath = Path.Combine(projectRootPath, "EmailTemplate/PasswordResetNotification.html");

                string fileContents = File.ReadAllText(confirmationEmailPath);
                fileContents = fileContents.Replace("##NAME##", $"{fullName}");
                fileContents = fileContents.Replace("##ACTIVATIONLINK##", callbackUrl);
                EmailLog logmodel = new EmailLog();
                logmodel.Receiver = "ayfadipe@gmail.com";
                logmodel.Sender = _mailsettings.MailFrom;
                logmodel.Subject = "Password Reset Notification";
                logmodel.MessageBody = fileContents;
                logmodel.DateCreated = logmodel.DateToSend = DateTime.Now;
                logmodel.IsSent = false;
                bool result = SendMail(logmodel.Subject, logmodel.Receiver, fileContents);
                if (result)
                {
                    logmodel.DateSent = DateTime.Now;
                    logmodel.IsSent = true;
                    logmodel.Retires++;
                }
                else
                {
                    logmodel.IsSent = false;
                    logmodel.Retires++;
                }
                _emailogRepoQuery.Insert(logmodel);
                _emailogRepoQuery.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
        public bool SendMail(string subject, string email, string content)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            try
            {
                message.IsBodyHtml = true;
                MailAddress fromAddress = new MailAddress("businessadmin@jnfx-emarkets.com", _mailsettings.MailFrom);
                message.From = fromAddress;
                message.To.Add(new MailAddress(email, email));

                message.Subject = subject;
                message.Body = content;
                message.IsBodyHtml = true;

                smtpClient.Host = _mailsettings.SMTPServer;
                int portno = 25;
                int.TryParse(_mailsettings.SMTPPORT, out portno);
                smtpClient.Port = portno;
                smtpClient.Credentials = new System.Net.NetworkCredential(_mailsettings.SMTPUserName, _mailsettings.SMTPPassword); //not neccessary
                smtpClient.EnableSsl = true;
                smtpClient.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
            finally
            {
                smtpClient.Dispose();
                message.Dispose();
            }
        }
    }
}
