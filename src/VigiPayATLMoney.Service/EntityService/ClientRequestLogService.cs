﻿using AutoMapper;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VigiPay.Orbit.Data.AppEntity;
using VigiPay.Orbit.Repository.CoreRepositories;
using VigiPay.Orbit.Repository.MongoDBRepo;
using VigiPay.Orbit.Service.IEntityService;

namespace VigiPay.Orbit.Service.EntityService
{
    public class ClientRequestLogService : IClientRequestLogService
    {

        private readonly IMongoDBRepository<ClientRequestLog, string> _RequestlogCommand;
        public ClientRequestLogService(IMongoDBRepository<ClientRequestLog, string> RequestlogCommand)
        {
            _RequestlogCommand = RequestlogCommand;
        }

        public void SaveClientReQuest(ClientRequestLog model)
        {
            _RequestlogCommand.Insert(model);
        }
    }
}
