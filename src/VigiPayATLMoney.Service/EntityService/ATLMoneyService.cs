﻿using AutoMapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VigiPay.ATLMoney.Data.AppEntity;
using VigiPay.ATLMoney.Data.AppViewModel;
using VigiPay.ATLMoney.Service.IEntityService;
using VigiPay.ATLMoney.Service.Utility;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.ATLMoney.Service.ViewModel.ATLMoney;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Service.ViewModel;

namespace VigiPay.ATLMoney.Service.EntityService
{
    public class ATLMoneyService : IATLMoneyService
    {
        private readonly ITransactionLogService _transactionLogService;
        private ATLMoneySetting _atlmoneysettings;
        private readonly IHttpClientFactory _clientFactory;
        private readonly IMapper _mapper;
        public ATLMoneyService(IHttpClientFactory clientFactory, ITransactionLogService transactionLogService, IOptions<ATLMoneySetting> settings, IMapper mapper)
        {
            _clientFactory = clientFactory;
            _atlmoneysettings = settings.Value;
            _transactionLogService = transactionLogService;
            _mapper = mapper;
        }
        public  async Task<APIResponseModel<SettlementRateResponseVm>> SettlementRate()
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var response = await client.GetAsync("/api/settlement-rates");
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<SettlementRateResponseVm>(result);
                    if (responseString== null)
                    {
                        return WebApiResponses<SettlementRateResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<SettlementRateResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<SettlementRateResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<SettlementRateResponseVm>.ErrorOccured(ex.Message);
            }
        }
        
        public async Task<APIResponseModel<AccountBalancesResponseVm>> AccountBalances()
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var response = await client.GetAsync("/api/balances");
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<AccountBalancesResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<AccountBalancesResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<AccountBalancesResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<AccountBalancesResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<AccountBalancesResponseVm>.ErrorOccured(ex.Message);
            }
        }
        
        public async Task<APIResponseModel<AccountBalanceByCurrencyVm>> AccountBalanceByCurrency(string currencyCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/balances/{currencyCode}";
                resourceURL = resourceURL.Replace("{currencyCode}", currencyCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<AccountBalanceByCurrencyVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<AccountBalanceByCurrencyVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<AccountBalanceByCurrencyVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<AccountBalanceByCurrencyVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<AccountBalanceByCurrencyVm>.ErrorOccured(ex.Message);
            }
        }
        
        public async Task<APIResponseModel<AccountStatementResponseVm>> AccountStatementByCurrency(string currencyCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/statement/{currencyCode}";
                resourceURL = resourceURL.Replace("{currencyCode}", currencyCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<AccountStatementResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<AccountStatementResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<AccountStatementResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<AccountStatementResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<AccountStatementResponseVm>.ErrorOccured(ex.Message);
            }
        }
        
        public async Task<APIResponseModel<BankResponseVm>> BankByCountryCode(string countryCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/banks?country={countryCode}";
                resourceURL = resourceURL.Replace("{countryCode}", countryCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<BankResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<BankResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<BankResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<BankResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<BankResponseVm>.ErrorOccured(ex.Message);
            }
        }

        public async Task<APIResponseModel<CollectionPointByCountryCodeResponseVm>> GetCashCollectionPointByCountryCode(string countryCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/cash-collection-partners?country={countryCode}";
                resourceURL = resourceURL.Replace("{countryCode}", countryCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<CollectionPointByCountryCodeResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<CollectionPointByCountryCodeResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<CollectionPointByCountryCodeResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<CollectionPointByCountryCodeResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<CollectionPointByCountryCodeResponseVm>.ErrorOccured(ex.Message);
            }
        }

        public async Task<APIResponseModel<GetCollectionPointResponseVm>> GetCollectionPointByCountryCodeByPartnerCode(string countryCode,string PartnerCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/collection-points?country={countryCode}&partner={PartnerCode}";
                resourceURL = resourceURL.Replace("{countryCode}", countryCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<GetCollectionPointResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<GetCollectionPointResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<GetCollectionPointResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<GetCollectionPointResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<GetCollectionPointResponseVm>.ErrorOccured(ex.Message);
            }
        }
        
        public async Task<APIResponseModel<MobileDeliveryResponseVm>> GetMobileDeliveryOptions(string countryCode)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/delivery-options?country={countryCode}&payoutMethod=MW";
                resourceURL = resourceURL.Replace("{countryCode}", countryCode);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<MobileDeliveryResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<MobileDeliveryResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<MobileDeliveryResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<MobileDeliveryResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<MobileDeliveryResponseVm>.ErrorOccured(ex.Message);
            }
        }


        public async Task<APIResponseModel<GetTransactionResponseVm>> GetAllTransaction()
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var response = await client.GetAsync("/api/transactions");
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<GetTransactionResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<GetTransactionResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<GetTransactionResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<GetTransactionResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<GetTransactionResponseVm>.ErrorOccured(ex.Message);
            }
        }


        public async Task<APIResponseModel<TransactionResponseVm>> GetTransaction(string TransactionId)
        {
            try
            {
                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);
                // execute the request
                var resourceURL = "/api/transactions/{TransactionId}";
                resourceURL = resourceURL.Replace("{TransactionId}", TransactionId);
                var response = await client.GetAsync(resourceURL);
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<TransactionResponseVm>(result);
                    if (responseString == null)
                    {
                        return WebApiResponses<TransactionResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    return WebApiResponses<TransactionResponseVm>.Successful(responseString);
                }
                else
                {
                    return WebApiResponses<TransactionResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return WebApiResponses<TransactionResponseVm>.ErrorOccured(ex.Message);
            }
        }


        public async Task<APIResponseModel<PayoutResponseVm>> PostTransaction(PayoutVm model)
        {
            TransactionLog transaction = null;

            //check if transactionRef exists
            if (_transactionLogService.ClientTransactionRefExists(model.TransactionReference))
                return WebApiResponses<PayoutResponseVm>.ErrorOccured("TransactionReference has already been used");

            //create transaction
            transaction = _transactionLogService.Create(model);
            var atlmoneyRequestmodel = _mapper.Map<PostTransactionVm>(transaction);

            //if (model.payout_method == "CP")
            //{
            //    if (string.IsNullOrEmpty(model.payout_partner))
            //    {
            //        return WebApiResponses<TransactionResponseVm>.ErrorOccured("If Payment method is cash pickup,cash collection Partner code is required");
            //    }
            //}
            if (!string.IsNullOrEmpty(atlmoneyRequestmodel.recipient.bank) && !string.IsNullOrEmpty(atlmoneyRequestmodel.recipient.iban))
            {
                atlmoneyRequestmodel.payout_method = "AC";
            }
            //else if (model.payout_method == "MW")
            //{
            //    if (string.IsNullOrEmpty(model.recipient.mobile_number))
            //    {
            //        return WebApiResponses<TransactionResponseVm>.ErrorOccured("If Payment method is Mobile Wallet,Recipient Mobile Number is required");
            //    }
            //}
            try
            {
                var dict = PostTransactionKeyPair(atlmoneyRequestmodel);

                var client = _clientFactory.CreateClient();
                client.BaseAddress = new Uri(_atlmoneysettings.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _atlmoneysettings.Token);


                var dtoString = JsonConvert.SerializeObject(atlmoneyRequestmodel);
                // StringContent httpContent = new StringContent(dtoString, Encoding.UTF8, "application/json");

                var response = await client.PostAsync("/api/transactions", new FormUrlEncodedContent(dict));
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    var responseString = JsonConvert.DeserializeObject<TransactionResponseVm>(result);

                    _transactionLogService.SaveLog(new ResponseLog
                    {
                        RequestType = EnumReQuestType.PayOut,
                        Response = result,
                        VigiPayRef = transaction.VigipayReference
                    });

                    if (responseString == null)
                    {
                        transaction.ResponseCode = "99";
                        transaction.ResponseMessage = "Gateway Processing Error";
                        transaction.TransactionStatus = (TransactionStatus)TransactionStatus.Failed;
                        transaction.StatusDescription = Util.GetDescription(transaction.TransactionStatus);
                        _transactionLogService.Update(transaction);
                        return WebApiResponses<PayoutResponseVm>.ErrorOccured("An error occured during account lookup");
                    }
                    transaction.ProviderReferenceNumber = responseString.transaction.id;
                    transaction.ProviderFXRate = responseString.transaction.exchange_rate;
                    transaction.TransactionStatus = (TransactionStatus)TransactionStatus.Processing;
                    transaction.StatusDescription = Util.GetDescription(transaction.TransactionStatus);
                    _transactionLogService.Update(transaction);
                    return WebApiResponses<PayoutResponseVm>.Successful(_mapper.Map<PayoutResponseVm>(responseString));
                }
                else
                {
                    transaction.ResponseCode = "99";
                    transaction.ResponseMessage = "Gateway Processing Error";
                    transaction.TransactionStatus = (TransactionStatus)TransactionStatus.Failed;
                    transaction.StatusDescription = Util.GetDescription(transaction.TransactionStatus);
                    _transactionLogService.Update(transaction);
                    return WebApiResponses<PayoutResponseVm>.ErrorOccured("Message:" + response.ReasonPhrase + " " + "Status Code:" + response.StatusCode);
                }
            }
            catch (Exception ex)
            {
                transaction.ResponseCode = "99";
                transaction.ResponseMessage = "Gateway Processing Error";
                transaction.TransactionStatus = (TransactionStatus)TransactionStatus.Failed;
                transaction.StatusDescription = Util.GetDescription(transaction.TransactionStatus);
                _transactionLogService.Update(transaction);
                Log.Error(ex);
                return WebApiResponses<PayoutResponseVm>.ErrorOccured(ex.Message);
            }
        }

        private Dictionary<string, string> PostTransactionKeyPair(PostTransactionVm model)
        {
            var dict = new Dictionary<string, string>();
            dict.Add("from_country", model.from_country);
            dict.Add("from_currency", model.from_currency);
            dict.Add("send_amount", model.send_amount.ToString());
            dict.Add("to_country", model.to_country);
            dict.Add("to_currency", model.to_currency);
            dict.Add("payout_amount", model.payout_amount.ToString());
            dict.Add("payout_method", model.payout_method);
            dict.Add("third_party_reference", model.third_party_reference);
            //customer
            dict.Add("customer[first_name]", model.Customer.first_name);
            dict.Add("customer[last_name]", model.Customer.last_name);
            dict.Add("customer[date_of_birth]", model.Customer.date_of_birth);
            dict.Add("customer[city]", model.Customer.city);
            dict.Add("customer[nationality]", model.Customer.nationality);
            dict.Add("customer[address]", model.Customer.address);
            dict.Add("customer[country]", model.Customer.country);
            dict.Add("customer[mobile_number]", model.Customer.mobile_number);

            //rece
            dict.Add("recipient[type]", model.recipient.type);
            dict.Add("recipient[first_name]", model.recipient.first_name);
            dict.Add("recipient[last_name]", model.recipient.last_name);
            dict.Add("recipient[bank]", model.recipient.bank);
            dict.Add("recipient[iban]", model.recipient.iban);
            // dict.Add("recipient[mobile_number]", model.recipient..ToString());
            dict.Add("recipient[relation]", model.recipient.relation);

            //rece
            dict.Add("purpose", model.purpose);
            dict.Add("message", model.message);
            dict.Add("poi_document", model.poi_document);
            dict.Add("poi_id_number", model.poi_id_number);
            dict.Add("poi_valid_from", model.poi_valid_from);
            dict.Add("poi_expiry", model.poi_expiry);
            return dict;
        }

    }
}

