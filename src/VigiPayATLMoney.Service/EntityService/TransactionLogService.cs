﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VigiPay.ATLMoney.Data.AppEntity;
using VigiPay.ATLMoney.Service.IEntityService;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.Orbit.Repository.CoreRepositories;

namespace VigiPay.ATLMoney.Service.EntityService
{
    public class TransactionLogService : ITransactionLogService
    {

        private readonly IRepositoryQuery<TransactionLog, long> _TransactionRepoQuery;
        private readonly IRepositoryCommand<TransactionLog, long> _TransactionCommandQuery;
        private readonly IRepositoryCommand<ResponseLog, long> _ResponseLogCommandQuery;
        private readonly IMapper _mapper;

        public TransactionLogService(IRepositoryQuery<TransactionLog, long> TransactionRepoQuery, IRepositoryCommand<TransactionLog, long> TransactionCommandQuery, IRepositoryCommand<ResponseLog, long> ResponseLogCommandQuery, IMapper mapper)
        {
            _TransactionRepoQuery = TransactionRepoQuery;
            _TransactionCommandQuery = TransactionCommandQuery;
            _ResponseLogCommandQuery = ResponseLogCommandQuery;
            _mapper = mapper;
        }

        public bool ClientTransactionRefExists(string clientTransactionRef)
        {
            return _TransactionRepoQuery.GetAll().Any(x => x.TransactionReference == clientTransactionRef);
        }

        public TransactionLog Create(PayoutVm model)
        {
            var transmodel = _mapper.Map<TransactionLog>(model);
            _TransactionCommandQuery.Insert(transmodel);
            _TransactionCommandQuery.SaveChanges();
            return transmodel;
        }

        public List<TransactionLog> GetAll(Expression<Func<TransactionLog, bool>> predicate)
        {
            return _TransactionRepoQuery.GetAllList(predicate);
        }

        public IQueryable<TransactionLog> GetAll()
        {
            return _TransactionRepoQuery.GetAll();
        }

        public TransactionLog GetSingle(Expression<Func<TransactionLog, bool>> predicate)
        {
            return _TransactionRepoQuery.Single(predicate);
        }

        public void SaveLog(ResponseLog model)
        {
            _ResponseLogCommandQuery.Insert(model);
            _ResponseLogCommandQuery.SaveChanges();
        }

        public void Update(TransactionLog model)
        {
            _TransactionCommandQuery.Update(model);
            _TransactionCommandQuery.SaveChanges();
        }
    }
}
