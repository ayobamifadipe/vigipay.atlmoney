﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VigiPay.Orbit.Data.AppViewModel;
using VigiPay.Orbit.Service.ViewModel.S3Amazon;

namespace VigiPay.Orbit.Service.EntityService
{
    public class S3Helper 
    {
        private string _accessKey;
        private string _accessCode;
        private static string _applicationFolder = "ApplicationData/VigiPay";
        private static string _rootBuket = "vgg-aws-tst-bucket";
        private static string _serviceURL = "https://s3-eu-west-1.amazonaws.com";
        private RegionEndpoint RegionEndpoint = RegionEndpoint.EUWest1;
       // private IAmazonS3 _amazonS3Client;

        public S3Helper(IOptions<AmazaonS3APPSetting> option)
        {
            _applicationFolder = option.Value.S3ApplicationFolder;
            _rootBuket = option.Value.S3BucketName;
            _serviceURL = option.Value.S3BucketServiceURL;
        }
        public void UploadDirectory(string folderPath, string targetFolderName)
        {
            using (TransferUtility directoryTransferUtility = new TransferUtility(new AmazonS3Client(RegionEndpoint)))
            {
                TransferUtilityUploadDirectoryRequest request =
                    new TransferUtilityUploadDirectoryRequest
                    {
                        BucketName = _rootBuket,
                        Directory = folderPath,
                        SearchOption = SearchOption.AllDirectories,
                        CannedACL = S3CannedACL.PublicRead,
                        KeyPrefix = string.Format("{0}/{1}", _applicationFolder, targetFolderName.ToLower())
                    };
                directoryTransferUtility.UploadDirectory(request);
            }
        }

        public void UploadFile(string sourcePath, string targetFullPath)
        {
            using (TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(RegionEndpoint)))
            {
                string key = string.Format("{0}/{1}", _applicationFolder, targetFullPath.ToLower());
                TransferUtilityUploadRequest fileTransRequest = new TransferUtilityUploadRequest()
                {
                    BucketName = _rootBuket,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    Key = key,
                    FilePath = sourcePath,

                    CannedACL = S3CannedACL.PublicRead, // make replacement possible
                };
                fileTransferUtility.Upload(fileTransRequest);
            }

        }

        public string UploadFileStream(string targetFullPath, Stream inputStream, double contentLength, string contentType)
        {
            var file = string.Format("{0}/{1}", _applicationFolder, targetFullPath.ToUpper());
            using (TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(RegionEndpoint)))
            {
                TransferUtilityUploadRequest fileTransRequest = new TransferUtilityUploadRequest()
                {
                    BucketName = _rootBuket,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    Key = file,
                    CannedACL = S3CannedACL.PublicRead,
                    InputStream = inputStream,
                    ContentType = contentType
                };
                fileTransferUtility.Upload(fileTransRequest);
            }
            return _serviceURL + "/" + file;
        }
        
        public async Task<UploadPhotoModel> UploadObject(IFormFile file)
        {
            try
            {
                // connecting to the client
                var client = new AmazonS3Client(RegionEndpoint);
                byte[] fileBytes = new Byte[file.Length];
                file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));
                // create unique file name for prevent the mess
                var fileName = Guid.NewGuid() + file.FileName;
                var xfile = string.Format("{0}/{1}", _applicationFolder, fileName.ToUpper());
                PutObjectResponse response = null;
                using (var stream = new MemoryStream(fileBytes))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = _rootBuket,
                        Key = xfile,
                        InputStream = stream,
                        ContentType = file.ContentType,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    response = await client.PutObjectAsync(request);
                };

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    // this model is up to you, in my case I have to use it following;
                    return new UploadPhotoModel
                    {
                        Success = true,
                        FileName = fileName
                    };
                }
                else
                {
                    // this model is up to you, in my case I have to use it following;
                    return new UploadPhotoModel
                    {
                        Success = false,
                        FileName = fileName
                    };
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&(amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Check the provided AWS Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred: " + amazonS3Exception.Message);
                }
            }
        }

        public  async Task<UploadPhotoModel> RemoveObject(String fileName)
        {
            var client = new AmazonS3Client(RegionEndpoint);
            var request = new DeleteObjectRequest
            {
                BucketName = _rootBuket,
                Key = string.Format("{0}/{1}", _applicationFolder, fileName.ToUpper())  
            };
            var response = await client.DeleteObjectAsync(request);
            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return new UploadPhotoModel
                {
                    Success = true,
                    FileName = fileName
                };
            }
            else
            {
                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = fileName
                };
            }
        }

        public async Task<bool> FileExist(string key)
        {
            try
            {
                using (var client = new AmazonS3Client(RegionEndpoint))
                {

                    //var response = await client.(_rootBuket, key, null);
                    //return response.Count > 0;

                    var respponse = await client.GetObjectMetadataAsync(_rootBuket, key, null);
                     return true;

                    //  return client..(string.Format("{0}/{1}", _applicationFolder, key.ToLower())).Result;
                }

            }
            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                return false;
            }
            
        }

        public async Task DeletAllFIles()
        {
            var files = GetAllFileList().Result;
            DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest();
            multiObjectDeleteRequest.BucketName = _rootBuket;
            foreach (var file in files)
            {
                multiObjectDeleteRequest.AddKey(file.Key, null);
            }
            using (var _amazonS3Client = new AmazonS3Client(RegionEndpoint))
            {
               await  _amazonS3Client.DeleteObjectsAsync(multiObjectDeleteRequest);
            }
        }


        public async Task<List<S3Object>> GetFiles(string folderPath)
        {
            List<S3Object> fileList = new List<S3Object>();
            using (var _amazonS3Client = new AmazonS3Client(RegionEndpoint))
            {
                var listObjectsRequest = new ListObjectsV2Request()
                {
                    BucketName = _rootBuket,
                    Prefix = string.Format("{0}/{1}/", _applicationFolder, folderPath.ToLower()),
                };
                ListObjectsV2Response response = await _amazonS3Client.ListObjectsV2Async(listObjectsRequest);
                // Process response.
                fileList = response.S3Objects;
            }
            return fileList;
        }

        public async Task<List<S3Object>> GetAllFileList()
        {
            List<S3Object> fileList = new List<S3Object>();
            using (var _amazonS3Client = new AmazonS3Client(RegionEndpoint))
            {
                var listObjectsRequest = new ListObjectsRequest()
                {
                    BucketName = _rootBuket,
                    Prefix = _applicationFolder,// string.Format("{0}/{1}/", _eduportalFolder, ),
                };
               return _amazonS3Client.ListObjectsAsync(listObjectsRequest).Result.S3Objects;
                
            }
        }

      
        
    }
}
