﻿using Hangfire;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Data.AppEntity;
using VigiPay.Orbit.Data.AppViewModel;
using VigiPay.Orbit.Repository.CoreRepositories;
using VigiPay.Orbit.Service.IEntityService;

namespace VigiPay.Orbit.Service.EntityService
{
    public class EmailSendingJob : IEmailSendingJob
    {
        private readonly IRepositoryCommand<EmailLog, int> _emailogRepoCommand;
        private readonly IRepositoryQuery<EmailLog, int> _emailogRepoQuery;
        private MailSetting _mailsettings;
        public EmailSendingJob(
             IRepositoryCommand<EmailLog,int> emailogRepoCommand,
             IRepositoryQuery<EmailLog, int> emailogRepoQuery
            , IOptions<MailSetting> mailsettings)
        {
            _emailogRepoQuery = emailogRepoQuery;
            _emailogRepoCommand = emailogRepoCommand;
            _mailsettings = mailsettings.Value;

        }
        public async Task Run(IJobCancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            await SendPendingEmail();
        }

        public async Task SendPendingEmail()
        {
            List<EmailLog> emailLogs = _emailogRepoQuery.GetAllList(x => x.IsSent == false);
            if(emailLogs!= null)
            {
                foreach (EmailLog logmodel in emailLogs)
                {
                    bool result = SendMail(logmodel.Subject, logmodel.Receiver, logmodel.MessageBody);
                    if (result)
                    {
                        logmodel.DateSent = DateTime.Now;
                        logmodel.IsSent = true;
                        logmodel.Retires++;
                    }
                    else
                    {
                        logmodel.IsSent = false;
                        logmodel.Retires++;
                    }
                    _emailogRepoCommand.Update(logmodel);
                    _emailogRepoCommand.SaveChanges();
                    // var result = await CoreServices.MessageServices.SendPayferEmailAsync("Email Verification Notification", email, fileContents);

                }
            }
            
        }

        public bool SendMail(string subject, string email, string content)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            try
            {
                message.IsBodyHtml = true;
                MailAddress fromAddress = new MailAddress(_mailsettings.MailFrom, "ORBIT");
                message.From = fromAddress;
                message.To.Add(new MailAddress(email));

                message.Subject = subject;
                message.Body = subject;
                message.IsBodyHtml = true;

                smtpClient.Host = _mailsettings.SMTPServer;
                int portno = 25;
                int.TryParse(_mailsettings.SMTPPORT, out portno);
                smtpClient.Port = portno;
                smtpClient.Credentials = new System.Net.NetworkCredential(_mailsettings.SMTPUserName, _mailsettings.SMTPPassword); //not neccessary
                smtpClient.EnableSsl = true;
                smtpClient.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
            finally
            {
                smtpClient.Dispose();
                message.Dispose();
            }
        }
    }
}
