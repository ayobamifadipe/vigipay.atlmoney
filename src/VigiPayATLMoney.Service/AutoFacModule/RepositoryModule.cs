﻿using Autofac;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Repository;
using VigiPay.Orbit.Repository.CoreRepositories;
using VigiPay.Orbit.Repository.MongoDBRepo;
using VigiPay.Orbit.Service.AspNetCoreHelper;
using VigiPay.Orbit.Service.AuditHelper;

namespace VigiPay.Orbit.Service.AutoFacModule
{
    public class RepositoryModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<APPContext>().InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(RepositoryCommand<,>))
              .As(typeof(IRepositoryCommand<,>))
              .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(RepositoryQuery<,>))
               .As(typeof(IRepositoryQuery<,>))
               .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(MongoDBRepository<,>))
              .As(typeof(IMongoDBRepository<,>))
              .InstancePerLifetimeScope();

            //builder.RegisterGeneric(typeof(HttpContextClientInfoProvider))
            //.As(typeof(IClientInfoProvider))
            //.InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(IAutoDependencyRegister).Assembly)
                .AssignableTo<IAutoDependencyRegister>()
                .As<IAutoDependencyRegister>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            
            builder.RegisterAssemblyTypes(typeof(IAutoDependencyService).Assembly)
             .AssignableTo<IAutoDependencyService>()
             .As<IAutoDependencyService>()
             .AsImplementedInterfaces().InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}