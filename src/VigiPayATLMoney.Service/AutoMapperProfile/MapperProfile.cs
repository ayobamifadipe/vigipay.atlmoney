﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VigiPay.ATLMoney.Data.AppEntity;
using VigiPay.ATLMoney.Service.Utility;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.ATLMoney.Service.ViewModel.ATLMoney;

namespace VigiPay.Orbit.Service.AutoMapperProfile
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {

            CreateMap<PayoutVm, TransactionLog>()
           .ForMember(x => x.TransactionReference, y => { y.MapFrom(p => p.TransactionReference); })
           .ForMember(x => x.VigipayReference, y => { y.MapFrom(p => Util.TimeStampGuidCode("VGP")); })
           .ForMember(x => x.TransactionStatus, y => { y.MapFrom(p => TransactionStatus.Accepted); })
           .ForMember(x => x.StatusDescription, y => { y.MapFrom(p => Util.GetDescription(TransactionStatus.Accepted)); })
           .ForMember(x => x.RequestString, y => { y.MapFrom(p => Util.SerializeJson(p)); }).ReverseMap();


            CreateMap<TransactionLog, PostTransactionVm>()
          .ForMember(x => x.from_country, y => { y.MapFrom(p => p.SenderCountry); })
          .ForMember(x => x.from_currency, y => { y.MapFrom(p => p.SendCurrency); })
          .ForMember(x => x.send_amount, y => { y.MapFrom(p => p.SendAmount); })
          .ForMember(x => x.to_country, y => { y.MapFrom(p => p.BeneficiaryCountry); })
          .ForMember(x => x.to_currency, y => { y.MapFrom(p => p.BeneficiaryCurrency); })
          .ForMember(x => x.payout_method, y => { y.MapFrom(p => p.SendAmount); })
          .ForMember(x => x.purpose, y => { y.MapFrom(p => "INVST"); })
          .ForMember(x => x.message, y => { y.MapFrom(p => "Transaction form " + p.SenderFirstName + " " + p.SenderLastName); })
          .ForMember(x => x.third_party_reference, y => { y.MapFrom(p => p.VigipayReference); })
           //customer
          .ForMember(x => x.Customer.first_name, y => { y.MapFrom(p => p.SenderFirstName); })
          .ForMember(x => x.Customer.last_name, y => { y.MapFrom(p => p.SenderLastName); })
          .ForMember(x => x.Customer.date_of_birth, y => { y.MapFrom(p =>string.Empty); })
          .ForMember(x => x.Customer.city, y => { y.MapFrom(p => p.SenderCity); })
          .ForMember(x => x.Customer.nationality, y => { y.MapFrom(p => p.SenderCountry); })
          .ForMember(x => x.Customer.address, y => { y.MapFrom(p => p.SenderAddress); })
          .ForMember(x => x.Customer.country, y => { y.MapFrom(p => p.SenderCountry); })
          .ForMember(x => x.Customer.mobile_number, y => { y.MapFrom(p => p.SenderPhoneNumber); })


          //recipientVm
          .ForMember(x => x.recipient.type, y => { y.MapFrom(p => "IND"); })
          .ForMember(x => x.recipient.relation, y => { y.MapFrom(p => "BIZ"); })
          .ForMember(x => x.recipient.first_name, y => { y.MapFrom(p => p.BeneficiaryFirstName); })
          .ForMember(x => x.recipient.last_name, y => { y.MapFrom(p => p.BeneficiaryLastName); })
          .ForMember(x => x.recipient.iban, y => { y.MapFrom(p => p.BeneficiaryBankAccountNumber); })
          .ForMember(x => x.recipient.bank, y => { y.MapFrom(p => p.BeneficiaryBankRoutingNumber); })


          .ForMember(x => x.poi_document, y => { y.MapFrom(p => p.SenderDocument); })
          .ForMember(x => x.poi_id_number, y => { y.MapFrom(p => p.SenderDocumentIDNumber); })
           .ForMember(x => x.poi_valid_from, y => { y.MapFrom(p => p.SenderDocumentIssuedDate); })
          .ForMember(x => x.poi_expiry, y => { y.MapFrom(p => p.SenderDocumentExpiryDate); })

          .ReverseMap();

            CreateMap<TransactionLog, PayoutResponseVm>().AfterMap((src, dest) =>
            {
                dest.Amount = src.SendAmount.ToString();
                dest.TransactionReference = src.TransactionReference;
                dest.TransactionStatusCode = ((int)src.TransactionStatus).ToString();
                dest.TransactionStatusDescription = src.StatusDescription;
                dest.VigipayReference = src.VigipayReference;
                dest.ResponseCode = src.ResponseCode;
                dest.ResponseMessage = src.ResponseMessage;
                dest.ServiceType = src.ServiceType;
                dest.BeneficiaryCurrency = src.BeneficiaryCurrency;
            });

        }
    }
 }
