﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VigiPay.Orbit.Service.ViewModel.Corporate
{
    public class CorporateSignUpVm
    {
        [Required(ErrorMessage = "The Corporate Name is required")]
        public string Name { get;set;}

        [StringLength(4)]
        [MinLength(3)]
        public string Code { get; set; }
        [Required(ErrorMessage = "The Contact First Name is required")]
        public string ContactFirstName { get; set; }
        [Required(ErrorMessage = "The Contact Last Name is required")]
        public string ContactLastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid Email Address")]
        public string ContactEmail { get; set; }
        

        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [RegularExpression(@"^(?:(?=.*[a-z])(?=.*[$-/:-?{-~!^_`\[\]])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$", ErrorMessage = "Password should be a minimum of 8 characters with a number, an upper case letter, a lower case and a special character")]
        [Required(ErrorMessage = "The Password is required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Password cannot be less than 6 characters")]
        public string Password { get; set; }

        
        [Required(ErrorMessage = "The Confirm password is required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Password cannot be less than 6 characters")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
