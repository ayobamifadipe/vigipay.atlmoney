﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel
{
    public class PayoutResponseVm
    {
        public string VigipayReference { get; set; }
        public string TransactionStatusDescription { get; set; }
        public string TransactionStatusCode { get; set; }
        public string TransactionReference { get; set; }
        public string Amount { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string ServiceType { get; set; }
        public string BeneficiaryCurrency { get; set; }
    }
}
