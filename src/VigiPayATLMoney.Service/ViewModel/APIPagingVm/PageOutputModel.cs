﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Service.ViewModel.APIPagingVm
{
    public class PageOutputModel<T> where T : class
    {
        public PagingHeader Paging { get; set; }
        public List<LinkInfo> Links { get; set; }
        public List<T> Items { get; set; }
    }
}
