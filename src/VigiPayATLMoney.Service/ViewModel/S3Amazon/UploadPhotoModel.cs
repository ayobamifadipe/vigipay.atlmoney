﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.Orbit.Service.ViewModel.S3Amazon
{
    public  class UploadPhotoModel
    {
        public bool Success { get; set; }
        public string FileName { get; set; }
    }
}
