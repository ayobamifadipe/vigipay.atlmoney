﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class TransactionResponseVm
    {
        public string message { get; set; }
        public Transaction transaction { get; set; }
    }


    public class TransactionCustomer
    {
        public long id { get; set; }
        public string first_name { get; set; }
        public object middle_name { get; set; }
        public string last_name { get; set; }
        public string date_of_birth { get; set; }
        public string birth_city { get; set; }
        public string birth_country { get; set; }
        public string nationality { get; set; }
        public string birth_nationality { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string mobile_number { get; set; }
        public object phone_number { get; set; }
        public object email_address { get; set; }
    }

    public class TransactionRecipient
    {
        public long id { get; set; }
        public string full_name { get; set; }
        public string type { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string iban { get; set; }
        public string bank { get; set; }
        public string mobile_number { get; set; }
        public string relation { get; set; }
    }

    public class Transaction
    {
        public string id { get; set; }
        public string from_country { get; set; }
        public string from_currency { get; set; }
        public double send_amount { get; set; }
        public string to_country { get; set; }
        public string to_currency { get; set; }
        public double payout_amount { get; set; }
        public string payout_method { get; set; }
        public string payout_partner { get; set; }
        public string exchange_rate { get; set; }
        public double fees { get; set; }
        public string settlement_currency { get; set; }
        public double settlement_amount { get; set; }
        public double commission { get; set; }
        public double total_settlement { get; set; }
        public long delivery_reference { get; set; }
        public string third_party_reference { get; set; }
        public TransactionCustomer customer { get; set; }
        public TransactionRecipient recipient { get; set; }
        public string status { get; set; }
        public string purpose { get; set; }
        public string poi_document { get; set; }
        public string poi_id_number { get; set; }
        public string poi_valid_from { get; set; }
        public string poi_expiry { get; set; }
        public string message { get; set; }
        public DateTime created_on { get; set; }
    }
}
