﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class AccountBalancesResponseVm
    {
        public string message { get; set; }
        public Balances balances { get; set; }
    }

    public class Balances
    {
        public string GBP { get; set; }
        public string EUR { get; set; }
    }

}
