﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class GetCollectionPointResponseVm
    {
        public string message { get; set; }
        public List<Location> locations { get; set; }
    }

    public class BusinessHours
    {
        public DayObjevtVm monday { get; set; }
        public DayObjevtVm tuesday { get; set; }
        public DayObjevtVm wednesday { get; set; }
        public DayObjevtVm thursday { get; set; }
        public DayObjevtVm friday { get; set; }
        public DayObjevtVm saturday { get; set; }
        public DayObjevtVm sunday { get; set; }
    }

    public class Location
    {
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string postcode { get; set; }
        public List<object> emails { get; set; }
        public List<string> contacts { get; set; }
        public string information { get; set; }
        public BusinessHours business_hours { get; set; }
    }


    public class DayObjevtVm
    {
        public string opens { get; set; }
        public string closes { get; set; }
    }

}
