﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{

    public static class EnumExtensions
    {
        /// <summary>
        /// Retrieves the <see cref="DisplayAttribute.Name" /> property on the <see cref="DisplayAttribute" />
        /// of the current enum value, or the enum's member name if the <see cref="DisplayAttribute" /> is not present.
        /// </summary>
        /// <param name="val">This enum member to get the name for.</param>
        /// <returns>The <see cref="DisplayAttribute.Name" /> property on the <see cref="DisplayAttribute" /> attribute, if present.</returns>
        public static string GetDisplayName(this Enum val)
        {
            return val.GetType()
                      .GetMember(val.ToString())
                      .FirstOrDefault()
                      ?.GetCustomAttribute<DisplayAttribute>(false)
                      ?.Name
                      ?? val.ToString();
        }
    }

    public  enum PaymentMethodEnum
    {
        [Display(Name = "Account Transfer")]
        AC=1,
        [Display(Name = "Cash Pickup")]
        CP,
        [Display(Name = "Home Delivery")]
        HD,
        [Display(Name = "Mobile Wallet")]
        MW

    }
    public class PostTransactionVm
    {
        public PostTransactionVm()
        {
            purpose = "INVST";
        }
        [Required]
        public string from_country { get; set; }

        [Required]
        public string from_currency { get; set; }

        [Required]
        public decimal send_amount { get; set; }

        [Required]
        public string to_country { get; set; }

        [Required]
        public string to_currency { get; set; }
        [Required]
        public decimal payout_amount { get; set; }

        [Required]
        public string payout_method { get; set; }

        [Required]
        public string purpose { get; set; }
        public string message { get; set; }

        //public string payout_partner { get; set; }

        //[Required]
        //public decimal exchange_rate { get; set; }
        //[Required]
        // public decimal fees { get; set; }
        [Required]
        public string third_party_reference { get; set; }

        public  CustomerVm Customer { get; set; }


        public string poi_document { get; set; }
        public string poi_id_number { get; set; }
        public string poi_valid_from { get; set; }
        public string poi_expiry { get; set; }

        public recipientVm recipient { get; set; }

      
       // public string poi_file { get; set; }

    }


    public class CustomerVm
    {
        [Display(Name = "first_name")]
        public string first_name { get; set; }
        [Display(Name = "last_name")]
        public string last_name { get; set; }
        [Display(Name = "date_of_birth")]
        public string date_of_birth { get; set; }
        [Display(Name = "city")]
        public string city { get; set; }
        [Display(Name = "nationality")]
        public string nationality { get; set; }
        [Display(Name = "address")]
        public string address { get; set; }
        [Display(Name = "country")]
        public string country { get; set; }

        [Display(Name = "mobile_number")]
        public string mobile_number { get; set; }

   
    }

    public class recipientVm
    {
        public recipientVm()
        {
            type = "IND";
            relation = "BIZ";
        }

        [Display(Name = "type")]
        public string type { get; set; }

        [Display(Name = "relation")]
        public string relation { get; set; }

        [Display(Name = "first_name")]

        public string first_name { get; set; }
        [Display(Name = "last_name")]
        public string last_name { get; set; }

        //[Display(Name = "business_name")]
        //public string business_name { get; set; }
        
        [Display(Name = "iban")]
        public string iban { get; set; }

        [Display(Name = "bank")]
        public string bank { get; set; }

        //[Display(Name = "mobile_number")]
        //public string mobile_number { get; set; }

 
    }
}
