﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class AccountStatementResponseVm
    {
        public string message { get; set; }
        public List<Entry> entries { get; set; }
        public int current_entries { get; set; }
        public int total_entries { get; set; }
        public int page { get; set; }
        public int total_pages { get; set; }
    }


    public class Entry
    {
        public string id { get; set; }
        public string transaction_number { get; set; }
        public string delivery_reference { get; set; }
        public string narration { get; set; }
        public string amount { get; set; }
        public string commission { get; set; }
        public DateTime created { get; set; }
    }
}
