﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class MobileDeliveryResponseVm
    {
        public string message { get; set; }
        public List<DeliveryOption> delivery_options { get; set; }
    }

    public class DeliveryOption
    {
        public string code { get; set; }
        public string title { get; set; }
        public string customer_identifier_label { get; set; }
    }
}
