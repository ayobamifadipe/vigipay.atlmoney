﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class BankResponseVm
    {
        public string message { get; set; }
        public List<Bank> banks { get; set; }
    }

    public class Bank
    {
        public string code { get; set; }
        public string title { get; set; }
    }
}
