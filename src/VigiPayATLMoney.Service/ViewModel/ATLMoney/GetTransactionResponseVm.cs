﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{

    public class GetTransactionObject 
    {
        public string id { get; set; }
        public string from_country { get; set; }
        public string from_currency { get; set; }
        public int send_amount { get; set; }
        public string to_country { get; set; }
        public string to_currency { get; set; }
        public int payout_amount { get; set; }
        public string payout_method { get; set; }
        public string payout_partner { get; set; }
        public string exchange_rate { get; set; }
        public double fees { get; set; }
        public string settlement_currency { get; set; }
        public double settlement_amount { get; set; }
        public double commission { get; set; }
        public double total_settlement { get; set; }
        public object delivery_reference { get; set; }
        public string third_party_reference { get; set; }
        public string status { get; set; }
        public DateTime created_on { get; set; }
    }

    public class GetTransactionResponseVm
    {
        public string message { get; set; }
        public List<GetTransactionObject> transactions { get; set; }
        public int current_transactions { get; set; }
        public int total_transactions { get; set; }
        public int page { get; set; }
        public int total_pages { get; set; }
    }
}
