﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class Rate
    {
        public string from { get; set; }
        public string to { get; set; }
        public double rate { get; set; }
    }

    public class SettlementRateResponseVm
    {
        public string message { get; set; }
        public List<Rate> rates { get; set; }
    }
    
}
