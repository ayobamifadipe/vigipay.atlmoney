﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class CollectionPointByCountryCodeResponseVm
    {
        public string message { get; set; }
        public List<Partner> partners { get; set; }
    }

    public class Partner
    {
        public string code { get; set; }
        public string title { get; set; }
        public string information { get; set; }
    }

}
