﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel.ATLMoney
{
    public class AccountBalanceByCurrencyVm
    {
        public string message { get; set; }
        public string balance { get; set; }
    }
}
