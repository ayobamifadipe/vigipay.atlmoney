﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel
{
    public class NameValuePair
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
