﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VigiPay.ATLMoney.Service.ViewModel
{
    public class PayoutVm
    {
        [Required]
        public string SenderFirstName { get; set; }
        [Required]
        public string SenderLastName { get; set; }
        public string SenderCountry { get; set; }
        public string SenderEmail { get; set; }
        [Required]
        public string SenderPhoneNumber { get; set; }
        public string SenderAddress { get; set; }
        public string SenderCity { get; set; }
        [Required]
        public string SendAmount { get; set; }
        [Required]
        public string SendCurrency { get; set; }
        [Required]
        public string SenderDocument { get; set; }
        [Required]
        public string SenderDocumentIDNumber { get; set; }
        [Required]
        public string SenderDocumentIssuedDate { get; set; }
        [Required]
        public string SenderDocumentExpiryDate { get; set; }

        //Receiver
        [Required]
        public string BeneficiaryFirstName { get; set; }
        [Required]
        public string BeneficiaryLastName { get; set; }
        [Required]
        public string BeneficiaryPhoneNumber { get; set; }
        [Required]
        public string BeneficiaryCountry { get; set; }
        public string BeneficiaryAddress { get; set; }
        public string BeneficiaryEmail { get; set; }

        public string BeneficiaryMobileOperator { get; set; }
        public string BeneficiaryBankRoutingNumber { get; set; }
        public string BeneficiaryBankAccountNumber { get; set; }

        public string BeneficiaryCurrency { get; set; }

        public string ServiceType { get; set; }

        public string TransactionReference { get; set; }
    }
}
