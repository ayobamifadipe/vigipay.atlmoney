﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NLog.Extensions.Logging;
using NLog.Web;
using StackifyLib;
using StackifyLib.CoreLogger;
using Swashbuckle.AspNetCore.Swagger;
using VigiPay.ATLMoney.Data.AppViewModel;
using VigiPay.Orbit.ClientAPI.HangfireSettingHelper;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Data.AppViewModel;
using VigiPay.Orbit.Repository.MongoDBRepo;
using VigiPay.Orbit.Service.AspNetCoreHelper;
using VigiPay.Orbit.Service.AuditHelper;
using VigiPay.Orbit.Service.AutoFacModule;
using VigiPay.Orbit.Service.EntityService;
using VigiPay.Orbit.Service.IEntityService;
using VigiPay.Orbit.Service.Utility;

namespace VigiPay.Orbit.ClientAPI
{
    internal static class AbpMvcOptionsExtensions
    {
        public static void AddFilterOptions(this MvcOptions options, IServiceCollection services)
        {
            AddActionFilters(options);
        }

        private static void AddActionFilters(MvcOptions options)
        {
            options.Filters.AddService(typeof(APIClientRequestFilter));
        }
    }

    public class Startup
    {
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}
        private string ContentRootPath;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            ContentRootPath = env.ContentRootPath;
            var config = builder.Build();
            Configuration = config;
            ConfigurationRoot = config;

            StackifyLib.Config.Environment = env.EnvironmentName; //optional
        }

        public IConfiguration Configuration { get; }
        public IConfigurationRoot ConfigurationRoot { get; }

        //public IContainer ApplicationContainer { get; private set; }


     
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                 .AddJsonOptions(options =>
                 {
                     options.SerializerSettings.Formatting = Formatting.Indented;
                     options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                 });
                //.AddMvcOptions(options =>
                //{
                //    options.Filters.Add<APIClientRequestFilter>();
                //});

            services.AddCors();
            services.AddAutoMapper();
            services.AddHttpClient();
            // Register the Swagger generator, defining 1 or more Swagger documents

            ////Configure MVC
            //services.Configure<MvcOptions>(mvcOptions =>
            //{
            //    mvcOptions.Filters.AddService(typeof(APIClientRequestFilter));
            //    //mvcOptions.AddFilterOptions(services);
            //});

            // Register the ConfigurationBuilder instance of AuthSettings
            var authSettings = Configuration.GetSection(nameof(AppSettings));
            services.Configure<AppSettings>(authSettings);

            //var MongoDBSettings = Configuration.GetSection(nameof(MongoDBSetting));
            //services.Configure<MongoDBSetting>(MongoDBSettings);


            var APPURLSettings = Configuration.GetSection(nameof(APPURL));
            services.Configure<APPURL>(APPURLSettings);

            var MailSettings = Configuration.GetSection(nameof(MailSetting));
            services.Configure<MailSetting>(MailSettings);
            //AmazonS3Setting

            var AmazonS3Setting = Configuration.GetSection(nameof(AmazaonS3APPSetting));
            services.Configure<AmazaonS3APPSetting>(MailSettings);
            
            //ATLMoneySetting
            var ATLMoneySettings = Configuration.GetSection(nameof(ATLMoneySetting));
            services.Configure<ATLMoneySetting>(ATLMoneySettings);

            var connectionstring = Configuration.GetConnectionString("DefaultConnection");
            //DBContext
            services.AddDbContext<APPContext>(options =>
                options.UseSqlServer(connectionstring).
                EnableSensitiveDataLogging());

            //enabling hangfire
            services.AddHangfire(config =>
            {
                var options = new SqlServerStorageOptions
                {
                    PrepareSchemaIfNecessary = false,
                    QueuePollInterval=TimeSpan.FromMinutes(5)
                };
                config.UseSqlServerStorage(connectionstring);
            });

            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IClientInfoProvider, HttpContextClientInfoProvider>();
            //Commenting this out for Eng Team mgmt approval
            //services.AddSingleton<APIClientRequestFilter>();
            services.AddScoped<IEmailSendingJob, EmailSendingJob>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "VIGIPAY ORBIT Client API",
                    //Description = "My First ASP.NET Core Web API",
                    //TermsOfService = "None",
                    //Contact = new Contact() { Name = "Fadipe Ayobami", Email = "ayfadipe@gmail.com", Url = "www.Fadipeayobami.com" }
                });
                //c.DescribeAllEnumsAsStrings();
                // Configure Swagger to use the xml documentation file
                var xmlFile = Path.ChangeExtension(typeof(Startup).Assembly.Location, ".xml");
                c.IncludeXmlComments(xmlFile);

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = "apiKey"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                  {
                    { "Bearer", new string[] { } }
                  });
            });

            // register dependencies
            var builder = new ContainerBuilder();
            builder.Populate(services);
            //builder.RegisterModule<LoggerModule>();
             builder.RegisterModule<RepositoryModule>();
            var ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("Nlog.config");

            loggerFactory.AddStackify(); //add the provider
            app.ConfigureStackifyLogging(ConfigurationRoot); //configure settings and ASP.NET exception hooks

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseDeveloperExceptionPage();
            }
            //add NLog to ASP.NET Core
            loggerFactory.AddNLog();

            app.UseCors(x => x
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            //This line enables the app to use Swagger, with the configuration in the ConfigureServices method.
            app.UseSwagger();

            //This line enables Swagger UI, which provides us with a nice, simple UI with which we can view our API calls.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VIGIPAY ORBIT Client API");
               // c.RoutePrefix = "swagger/ui";
            });
            app.UseMvc();
            app.UseAuthentication();
            //The following line is also optional, if you required to monitor your jobs.
            //Make sure you're adding required authentication 
            app.UseHangfireDashboard();
            //app.UseHangfireDashboard("/appHangfire", new DashboardOptions
            //{
            //   Authorization= new[] {new HangfireDasnBoardAuthorizationFilter()}
            //});
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                WorkerCount = 1,
            });
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts =5 });
            HangfireJobScheduler.SchedulerRecurringJobs();
        }
    }
}
