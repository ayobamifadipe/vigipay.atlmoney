﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using VigiPay.ATLMoney.Service.IEntityService;
using VigiPay.ATLMoney.Service.ViewModel;
using VigiPay.ATLMoney.Service.ViewModel.ATLMoney;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Service.ViewModel;
using NameValuePair = VigiPay.ATLMoney.Service.ViewModel.NameValuePair;

namespace VigiPay.ATLMoney.ClientAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IATLMoneyService _moneyService;
        public  PaymentController(IATLMoneyService moneyService)
        {
            _moneyService = moneyService;
        }

        [HttpGet("GetPaymentMethod")]
        [ProducesResponseType(typeof(List<NameValuePair>), 200)]
        [ProducesResponseType(typeof(List<NameValuePair>), 400)]
        public ActionResult GetPaymentMethod()
        {
            try
            {
                var model = Enum.GetValues(typeof(PaymentMethodEnum))
                                   .Cast<PaymentMethodEnum>()
                                   .Select(e => new VigiPay.ATLMoney.Service.ViewModel.NameValuePair
                                   {
                                       Value = e.ToString(),
                                       Text = e.GetDisplayName().ToString(),
                                   });
                if (model == null) return BadRequest($"Payment Method status is empty.");
                return Ok(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        //CollectionPointByCountryCodeResponseV

        [HttpGet("GetCashCollectionPoint/{CountryCode}")]
        [ProducesResponseType(typeof(APIResponseModel<List<CollectionPointByCountryCodeResponseVm>>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<List<CollectionPointByCountryCodeResponseVm>>), 400)]
        public async Task<ActionResult> GetCashCollectionPointByCountryCode(string CountryCode)
        {
            return Ok(await _moneyService.GetCashCollectionPointByCountryCode(CountryCode));
        }

        [HttpGet("GetSettlementRate")]
        [ProducesResponseType(typeof(APIResponseModel<List<SettlementRateResponseVm>>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<List<SettlementRateResponseVm>>), 400)]
        public async Task<ActionResult>  GetSettlementRate()
        {
            return Ok(await _moneyService.SettlementRate());
        }

        [HttpGet("GetAccoutBalances")]
        [ProducesResponseType(typeof(APIResponseModel<List<AccountBalancesResponseVm>>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<List<AccountBalancesResponseVm>>), 400)]
        public async Task<ActionResult> GetAccoutBalances()
        {
            return Ok(await _moneyService.AccountBalances());
        }

        [HttpGet("AccountBalanceByCurrency/{CurrencyCode}")]
        [ProducesResponseType(typeof(APIResponseModel<List<AccountBalanceByCurrencyVm>>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<List<AccountBalanceByCurrencyVm>>), 400)]
        public async Task<ActionResult> GetAccountBalanceByCurrency(string CurrencyCode)
        {
            return Ok(await _moneyService.AccountBalanceByCurrency(CurrencyCode.Trim().ToUpper()));
        }

        [HttpGet("GetAccoutBalances/{CurrencyCode}")]
        [ProducesResponseType(typeof(List<AccountStatementResponseVm>), 200)]
        [ProducesResponseType(typeof(List<AccountStatementResponseVm>), 400)]
        public async Task<ActionResult> GetAccountStatementByCurrency(string CurrencyCode)
        {
            return Ok(await _moneyService.AccountStatementByCurrency(CurrencyCode.Trim().ToUpper()));
        }


        [HttpGet("GetBankByCountryCode/{CountryCode}")]
        [ProducesResponseType(typeof(APIResponseModel<List<BankResponseVm>>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<List<BankResponseVm>>), 400)]
        [ProducesResponseType(typeof(APIResponseModel<List<BankResponseVm>>), 400)]
        public async Task<ActionResult> GetBankByCountryCode(string CountryCode)
        {
            return Ok(await _moneyService.BankByCountryCode(CountryCode.Trim().ToUpper()));
        }


        [HttpPost("GetTransaction/{TransactionId}")]
        [ProducesResponseType(typeof(APIResponseModel<TransactionResponseVm>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<TransactionResponseVm>), 400)]
        public async Task<ActionResult> GetTransaction(string TransactionId)
        {
            return Ok(await _moneyService.GetTransaction(TransactionId));
        }
        
        [HttpPost("Payout")]
        [ProducesResponseType(typeof(PayoutResponseVm), 200)]
        [ProducesResponseType(typeof(PayoutResponseVm), 400)]
        public async Task<ActionResult> Payout([FromBody]PayoutVm model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(await _moneyService.PostTransaction(model));
        }

        [HttpPost("GetAllTransaction")]
        [ProducesResponseType(typeof(APIResponseModel<GetTransactionResponseVm>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<GetTransactionResponseVm>), 400)]
        public async Task<ActionResult> GetAllTransaction()
        {
            return Ok(await _moneyService.GetAllTransaction());
        }


        [HttpPost("Status")]
        [ProducesResponseType(typeof(APIResponseModel<GetTransactionResponseVm>), 200)]
        [ProducesResponseType(typeof(APIResponseModel<GetTransactionResponseVm>), 400)]
        public async Task<ActionResult>  Statushook()
        {
            return Ok(await _moneyService.GetAllTransaction());
        }


    }
}