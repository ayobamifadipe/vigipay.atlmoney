﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VigiPay.Orbit.Data;
using VigiPay.Orbit.Service.AspNetCoreHelper;

namespace VigiPay.Orbit.ClientAPI.Controllers
{
    public class ClientGet
    {
        public string Name { get; set; }
    }
    // [ServiceFilter(typeof(APIClientRequestFilter))]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        protected readonly ILogger<ValuesController> _logger;

        public ValuesController(ILogger<ValuesController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Creates a TodoItem.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///        "id": 1,
        ///        "name": "Item1",
        ///        "isComplete": true
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>A newly created TodoItem</returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>    
        [HttpGet]
        [ProducesResponseType(typeof(ClientGet), 200)]
        //[ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<IEnumerable<string>> Get()
        {
            _logger.LogInformation("log4net test",null);
            Log.Info2("log4net test");
            return new string[] { "value1", "value2" };
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            // GET api/values
            //[SwaggerResponse((int)System.Net.HttpStatusCode.OK, Type = typeof(RestOkResponse<Response>))]
            //[SwaggerResponse((int)System.Net.HttpStatusCode.InternalServerError, Type = typeof(RestErrorResponse))]
            //[SwaggerResponse((int)System.Net.HttpStatusCode.BadRequest, Type = typeof(RestErrorResponse))]
            //[SwaggerResponse((int)System.Net.HttpStatusCode.Forbidden, Type = typeof(RestErrorResponse))]
            //[SwaggerResponse((int)System.Net.HttpStatusCode.NotFound)]
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);
            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}

